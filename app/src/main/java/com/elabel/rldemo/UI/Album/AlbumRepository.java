package com.elabel.rldemo.UI.Album;

import com.elabel.rldemo.Data.API.Album.AlbumApiAdapter;
import com.elabel.rldemo.Data.API.Album.AlbumApiService;
import com.elabel.rldemo.Data.Model.Album;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumRepository {
    private AlbumApiAdapter albumApiAdapter;
    private AlbumApiService albumApiService;
    private MutableLiveData<List<Album>> albumLists;

    public AlbumRepository(){
        albumApiAdapter = new AlbumApiAdapter();
        albumApiService = albumApiAdapter.getAlbumService();
        albumLists = new MutableLiveData<>();
    }

    public LiveData<List<Album>>getAllAlbums(){
        albumApiService.getAllAlbums().enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                albumLists.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                //TODO Controlar failure;
            }
        });
        return albumLists;
    }
}
