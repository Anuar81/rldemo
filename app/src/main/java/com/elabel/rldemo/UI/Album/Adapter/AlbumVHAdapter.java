package com.elabel.rldemo.UI.Album.Adapter;

import android.view.View;
import android.widget.TextView;

import com.elabel.rldemo.Data.Model.Album;
import com.elabel.rldemo.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumVHAdapter extends RecyclerView.ViewHolder{
    @BindView(R.id.txtID)
    TextView txtAlbumID;
    @BindView(R.id.txtTitle)
    TextView txtAlbumTitle;

    public AlbumVHAdapter(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(Album album){
        txtAlbumID.setText(album.getId().toString());
        txtAlbumTitle.setText(album.getTitle());
    }


}
