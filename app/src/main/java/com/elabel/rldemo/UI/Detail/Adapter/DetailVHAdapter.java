package com.elabel.rldemo.UI.Detail.Adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elabel.rldemo.Data.Model.Photo;
import com.elabel.rldemo.R;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailVHAdapter extends RecyclerView.ViewHolder {
    public @BindView(R.id.txtPhotoAlbumID)
    TextView txtPhotoAlbumID;
    public @BindView(R.id.txtPhotoAlbumTitle)
    TextView txtPhotoTitle;
    public @BindView(R.id.imgAlbumImage)
    ImageView imgPhotoAlbumImage;

    public DetailVHAdapter(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(Photo photo){
        txtPhotoAlbumID.setText(photo.getId().toString());
        txtPhotoTitle.setText(photo.getTitle());
        //image
        Picasso.get().load(photo.getThumbnailUrl()).into(imgPhotoAlbumImage);
    }
}
