package com.elabel.rldemo.UI.Detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.elabel.rldemo.Data.Model.Photo;
import com.elabel.rldemo.R;
import com.elabel.rldemo.UI.Detail.Adapter.DetailAdapter;
import com.elabel.rldemo.UI.Detail.Adapter.DetailVHAdapter;
import com.elabel.rldemo.UI.Detail.Adapter.RvDetailListener;
import com.elabel.rldemo.UI.Detail.Fragment.DetailFragment;

import java.util.List;

public class DetailActivity extends AppCompatActivity implements RvDetailListener {
    public final static String ALBUM_ID = "albumID";

    @BindView(R.id.recyclerViewDetail)
    RecyclerView recyclerView;
    @BindView(R.id.frameContainerDetail)
    FrameLayout frameContainer;

    private DetailViewModel viewModel;
    private DetailAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Butterknife
        ButterKnife.bind(this);

        String id = getIntent().getStringExtra(ALBUM_ID);

        //ViewModel
        viewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        viewModel.updatePhotoList(id);

        //Recycler
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL, false));
        adapter = new DetailAdapter(this, this);
        recyclerView.setAdapter(adapter);

        //Observers
        viewModel.getAlbumPhotoList().observe(this, new Observer<List<Photo>>() {
            @Override
            public void onChanged(List<Photo> photos) {
                adapter.setPhotos(photos);
                adapter.notifyDataSetChanged();
            }
        });



    }

    @Override
    public void rvPhotoListener(Photo photo, DetailVHAdapter holder) {
        DetailFragment fragment = new DetailFragment();

        Bundle bundle = new Bundle();
        bundle.putString(DetailFragment.NAME_TRANSITION_ID, ViewCompat.getTransitionName(holder.txtPhotoAlbumID));
        bundle.putString(DetailFragment.NAME_TRANSITION_TITLE, ViewCompat.getTransitionName(holder.txtPhotoTitle));
        bundle.putString(DetailFragment.NAME_TRANSITION_IMAGE, ViewCompat.getTransitionName(holder.imgPhotoAlbumImage));
        bundle.putString(DetailFragment.ID, photo.getId().toString());
        bundle.putString(DetailFragment.TITLE, photo.getTitle());
        bundle.putString(DetailFragment.IMG_URL, photo.getUrl());

        fragment.setArguments(bundle);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.frameContainerDetail,fragment)
                .addToBackStack(null)
                .addSharedElement(holder.txtPhotoAlbumID,ViewCompat.getTransitionName(holder.txtPhotoAlbumID))
                .addSharedElement(holder.txtPhotoTitle,ViewCompat.getTransitionName(holder.txtPhotoTitle))
                .addSharedElement(holder.imgPhotoAlbumImage,ViewCompat.getTransitionName(holder.imgPhotoAlbumImage))
                .commit();
    }
}
