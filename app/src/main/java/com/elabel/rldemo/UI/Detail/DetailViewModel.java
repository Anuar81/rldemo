package com.elabel.rldemo.UI.Detail;

import com.elabel.rldemo.Data.Model.Photo;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class DetailViewModel extends ViewModel {
    private DetailRepository repository;
    private LiveData<List<Photo>>albumPhotoLivedata;

    public DetailViewModel() {
        this.repository = new DetailRepository();
    }

    public void updatePhotoList(String ID){
        albumPhotoLivedata = repository.getAllAlbumPhoto(ID);
    }

    public  LiveData<List<Photo>>getAlbumPhotoList(){
        return albumPhotoLivedata;
    }

}
