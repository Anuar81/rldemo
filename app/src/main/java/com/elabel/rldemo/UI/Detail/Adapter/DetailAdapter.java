package com.elabel.rldemo.UI.Detail.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elabel.rldemo.Data.Model.Photo;
import com.elabel.rldemo.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

public class DetailAdapter extends RecyclerView.Adapter<DetailVHAdapter> {
    List<Photo>photos;
    Context context;
    RvDetailListener listener;

    public DetailAdapter(Context context, RvDetailListener listener) {
        photos = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    @NonNull
    @Override
    public DetailVHAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.photos_album_item, parent,false);
        return new DetailVHAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailVHAdapter holder, int position) {
        final Photo photo = photos.get(position);
        holder.setData(photo);

        //para las transiciones
        ViewCompat.setTransitionName(holder.imgPhotoAlbumImage, String.valueOf("image"+position));
        ViewCompat.setTransitionName(holder.txtPhotoAlbumID, String.valueOf("id"+position));
        ViewCompat.setTransitionName(holder.txtPhotoTitle, String.valueOf("title"+position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.rvPhotoListener(photo, holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
