package com.elabel.rldemo.UI.Album;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.elabel.rldemo.Data.Model.Album;
import com.elabel.rldemo.R;
import com.elabel.rldemo.UI.Album.Adapter.AlbumAdapter;
import com.elabel.rldemo.UI.Album.Adapter.AlbumVHAdapter;
import com.elabel.rldemo.UI.Album.Adapter.RvAlbumListener;
import com.elabel.rldemo.UI.Detail.DetailActivity;
import com.elabel.rldemo.UI.Splash.SplashFragment;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements RvAlbumListener, MaterialSearchBar.OnSearchActionListener {
    @BindView(R.id.recyclerViewAlbums)
    RecyclerView recyclerView;
    @BindView(R.id.frameContainer)
    FrameLayout frameContainer;
    @BindView(R.id.searchBar)
    MaterialSearchBar searchBar;

    private AlbumViewModel viewModel;
    private AlbumAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Butterknife bind
        ButterKnife.bind(this);

        //Viewmodel
        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);

        if(!viewModel.getSplashView()){
            launchSplashFrag();
        }
        viewModel.updateAlbumsFromInet();


        //Recycler
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new AlbumAdapter(this, this);
        recyclerView.setAdapter(adapter);

        //Search
        searchBar.setOnSearchActionListener(this);
        searchBar.setCardViewElevation(4);


        //Observers
        viewModel.getAlbumsList().observe(this, new Observer<List<Album>>() {
            @Override
            public void onChanged(List<Album> albums) {
                viewModel.searchAndFilter("");
            }
        });

        viewModel.getAlbumsActualList().observe(this, new Observer<List<Album>>() {
            @Override
            public void onChanged(List<Album> albums) {
                adapter.setAlbums(albums);
                adapter.notifyDataSetChanged();
                searchBar.setVisibility(View.VISIBLE);
            }
        });

    }

    private void launchSplashFrag(){
        viewModel.setSplashView(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.frameContainer,new SplashFragment(),null);
        ft.commit();
    }
    //Recycler click Listener
    @Override
    public void rvListener(Album album, AlbumVHAdapter holder) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.ALBUM_ID, album.getId());
        startActivity(intent);
    }

    //Search
    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(!enabled){
            viewModel.searchAndFilter("");
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {
        viewModel.searchAndFilter(text.toString());
    }

    @Override
    public void onButtonClicked(int buttonCode) {
    }
}
