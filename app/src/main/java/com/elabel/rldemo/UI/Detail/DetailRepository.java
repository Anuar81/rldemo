package com.elabel.rldemo.UI.Detail;

import com.elabel.rldemo.Data.API.Photo.PhotoApiAdapter;
import com.elabel.rldemo.Data.API.Photo.PhotoApiService;
import com.elabel.rldemo.Data.Model.Photo;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailRepository {
    private PhotoApiAdapter photoApiAdapter;
    private PhotoApiService photoApiService;
    private MutableLiveData<List<Photo>>photoList;

    public DetailRepository() {
        photoApiAdapter = new PhotoApiAdapter();
        photoApiService = photoApiAdapter.getPhotoApiService();
        photoList = new MutableLiveData<>();
    }

    public LiveData<List<Photo>>getAllAlbumPhoto(String ID){
        photoApiService.getAllPhotosByAlbumID(ID).enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                photoList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                //TODO lo mismo controlar el failure y etc...
            }
        });
        return photoList;
    }
}
