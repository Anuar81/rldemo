package com.elabel.rldemo.UI.Detail.Adapter;

import com.elabel.rldemo.Data.Model.Photo;

public interface RvDetailListener {
    void rvPhotoListener(Photo photo, DetailVHAdapter holder);
}
