package com.elabel.rldemo.UI.Album.Adapter;

import com.elabel.rldemo.Data.Model.Album;

public interface RvAlbumListener {
    void rvListener(Album album, AlbumVHAdapter holder);
}
