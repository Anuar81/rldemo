package com.elabel.rldemo.UI.Album.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elabel.rldemo.Data.Model.Album;
import com.elabel.rldemo.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumVHAdapter> {
    List<Album>albums;
    Context context;
    RvAlbumListener listener;

    public AlbumAdapter(Context context, RvAlbumListener listener) {
        albums = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @NonNull
    @Override
    public AlbumVHAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.album_item, parent,false);
        return new AlbumVHAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumVHAdapter holder, int position) {
        final Album album = albums.get(position);
        holder.setData(album);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.rvListener(album,holder);
            }
        });

    }

    @Override
    public int getItemCount() {
        return albums.size();
    }
}
