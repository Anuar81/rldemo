package com.elabel.rldemo.UI.Album;

import com.elabel.rldemo.Data.Model.Album;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AlbumViewModel extends ViewModel {
    private Boolean splashView = false;
    private AlbumRepository repository;
    private LiveData<List<Album>>albumsLiveData;
    //la uso para mutar con los filtros o sin ellos.
    private MutableLiveData<List<Album>>albumsActualList = new MutableLiveData<>();

    public Boolean getSplashView() {
        return splashView;
    }

    public void setSplashView(Boolean splashView) {
        this.splashView = splashView;
    }

    public AlbumViewModel() {
        this.repository = new AlbumRepository();
    }

    public void updateAlbumsFromInet(){
        albumsLiveData = repository.getAllAlbums();
    }

    public void searchAndFilter(String key){
        if(key.isEmpty()){
            albumsActualList.setValue(albumsLiveData.getValue());
        }else{
            List<Album>temp = new ArrayList<>();
            for (Album album:albumsLiveData.getValue()
                 ) {
                if(album.getTitle().contains(key)){
                    temp.add(album);
                }
            }
            albumsActualList.postValue(temp);
        }
    }

    public LiveData<List<Album>>getAlbumsList(){
        return albumsLiveData;
    }

    public MutableLiveData<List<Album>> getAlbumsActualList() {
        return albumsActualList;
    }
}
