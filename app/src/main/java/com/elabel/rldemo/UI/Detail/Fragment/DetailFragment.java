package com.elabel.rldemo.UI.Detail.Fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elabel.rldemo.R;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {
    public final  static String NAME_TRANSITION_IMAGE = "nameTransitionImage";
    public final  static String NAME_TRANSITION_ID = "nameTransitionID";
    public final  static String NAME_TRANSITION_TITLE = "nameTransitionTitle";
    public final  static String ID = "id";
    public final  static String TITLE = "title";
    public final  static String IMG_URL = "urlPhoto";

    @BindView(R.id.txtPhotoAlbumID)
    TextView txtDFID;
    @BindView(R.id.txtPhotoAlbumTitle)
    TextView txtDFTitle;
    @BindView(R.id.imgAlbumImage)
    ImageView imgDFPhoto;


    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //seteo el tipo de transicion antes de crear la vista.
        setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        String nameTransitionImage = bundle.getString(NAME_TRANSITION_IMAGE);
        String nameTransitionID = bundle.getString(NAME_TRANSITION_ID);
        String nameTransitionTextTitle = bundle.getString(NAME_TRANSITION_TITLE);
        String id = bundle.getString(ID);
        String title = bundle.getString(TITLE);
        String imgURL = bundle.getString(IMG_URL);

        //seteo el nombre de la transicion
        txtDFID.setTransitionName(nameTransitionID);
        txtDFTitle.setTransitionName(nameTransitionTextTitle);
        imgDFPhoto.setTransitionName(nameTransitionImage);

        txtDFID.setText(id);
        txtDFTitle.setText(title);
        Picasso.get().load(imgURL).into(imgDFPhoto);

        return view;
    }

}
