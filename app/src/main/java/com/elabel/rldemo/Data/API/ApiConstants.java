package com.elabel.rldemo.Data.API;

public class ApiConstants {
    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";

    //Albums
    public static final String ALBUMS = "albums";

    //Photos
    public static final String PHOTOS = "photos";
    public static final String PHOTOS_PATH = "albumid";
}
