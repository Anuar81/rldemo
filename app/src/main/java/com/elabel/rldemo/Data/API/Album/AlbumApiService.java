package com.elabel.rldemo.Data.API.Album;

import com.elabel.rldemo.Data.API.ApiConstants;
import com.elabel.rldemo.Data.Model.Album;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AlbumApiService {
    @GET(ApiConstants.ALBUMS)
    Call<List<Album>>getAllAlbums();
}
