package com.elabel.rldemo.Data.API.Photo;

import com.elabel.rldemo.Data.API.ApiConstants;
import com.elabel.rldemo.Data.Model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PhotoApiService {
    @GET(ApiConstants.PHOTOS)
    Call<List<Photo>>getAllPhotosByAlbumID(@Query(ApiConstants.PHOTOS_PATH) String id);
}
